1. Інсталювати @angular/cli глобально і створити новий проект store.

```
npm install -g @angular/cli
ng new store --routing=false --style=scss 
```

2. Не додавати роутинг, стилями вибрати scss

3. Створити папку core(модуль), features, shared.

```
cd store
ng generate module core
ng generate module features
ng generate module shared
```

4. В core створити компоненти header, footer і сервіс authentication. Імпортувати core module в app module

```
ng generate component core/header
ng generate component core/footer
ng generate service core/authentication
```

5. Створити 2 feature модулі products i cart.

```
ng generate module features/products
ng generate module features/cart
```

6. Всередині products створити компонент product, у якого changeDetection буде OnPush(використовуючи опції в cli)

```
ng generate component features/products/product --change-detection OnPush
```

7. В shared додати компонент button i input. Обидва компоненти мають бути з інлайновими стилями(не окремим файлом, а одразу в ts компоненті)

```
ng generate component shared/button --inline-style
ng generate component shared/input --inline-style
```

8. Використати button i input компоненті в products.

9. Використати input компонент в cart



використовувати 1 модуль на 1 компонент(виключення - core module)
